# Amcom 

## Project structure
Under development.


## Prerequisites
- Django==3.0.2
- djangorestframework==3.11.0
- psycopg2-binary==2.8.4
- python-decouple==3.3
- dj_database_url==0.5.0
- django-cors-headers==3.2.1
- asgiref==3.2.3
- astroid==2.3.3
- autopep8==1.4.4
- isort==4.3.21
- lazy-object-proxy==1.4.3
- Markdown==3.1.1
- mccabe==0.6.1
- pycodestyle==2.5.0
- pylint==2.4.4
- pytz==2019.3
- six==1.13.0
- sqlparse==0.3.0
- wrapt==1.11.2
- pytest==5.3.4
- pytest-cov==2.8.1
- pytest-django==3.8.0
- spytest-pythonpath==0.7.3
- codecov==2.0.15
- drf-yasg==1.17.0


- [Quick start](#quickstart)

### Postman

```
https://www.postman.com/joint-operations-cosmonaut-34701805/workspace/projetos/collection/25626587-736b00f6-9b9d-41e8-99f8-b33d076f6772?action=share&creator=25626587
```

### Docker

### Instale docker e docker-compose

Instale o `docker` e `docker-compose` segundo a documentação oficial.

No Ubuntu, as linhas abaixo podem ser usadas para instalar ambos.
Porém, versões mais atualizadas do `docker-compose` podem ser encontradas no [repositório oficial](https://github.com/docker/compose) :

```
# docker-ce-edge:
curl -L  "https://get.docker.com"  -o get-docker.sh
sh get-docker.sh
sudo usermod -aG docker `whoami`

# docker-compose:
sudo curl -L "https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

Você deve precisar reiniciar o seu computador para que o Docker Daemon inicie corretamente.

## Como executar o projeto usando o docker
Você precisa do docker e do docker-compose instalados. (Eles precisam correr sem sudo)
Eu uso as versões mais recentes deles.

- Run docker compose
```console
docker-compose up --build
```

### Development

## Como testar e executar o back-end do projeto localmente (sem docker)


- Crie um ambiente virtual Python. (Eu uso pyenv, então aqui 'python' é um link para python 3.8.3)
```console
python -m venv .venv
```

- Usar ambiente virtual.
```console
source .venv/bin/activate
```

- Instalar dependencias e rodar as migrations. 
```console
make local_start
```

- Debug tests.
```console
make test
```

- Debug backend
```console
make runserver
```

## Como testar e executar o frontend do projeto localmente (sem docker)

Você precisa de node instalado e npm ou yarn. (Abaixo passos use yarn).
Você precisa executar o back-end novamente ou o frontend não funciona da maneira correta.


- Instalar dependencias usando yarn. 
```console
yarn 
```

- Debug frontend
```console
yarn start
```

- Debug Tests
```console
yarn test
```
